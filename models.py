"""
This file defines the database models
"""

import datetime
from .common import db, Field, auth
from pydal.validators import *


def get_user_email():
    return auth.current_user.get('email') if auth.current_user else None

def get_time():
    return datetime.datetime.utcnow()


### Define your table below
#
# db.define_table('thing', Field('name'))
#
## always commit your models to avoid problems later

db.define_table(
    'account',
    Field('tag', 'string', requires=IS_NOT_EMPTY()),
    Field('phone', 'string', requires=IS_NOT_EMPTY()),
    Field('dob', 'date', requires=IS_NOT_EMPTY()),
    Field('ssn', 'string', requires=IS_NOT_EMPTY()),
    Field('state', 'string', requires=IS_NOT_EMPTY()),
    Field('bill_ad', 'string', requires=IS_NOT_EMPTY()), # stands for billing address
    Field('user_id', 'reference auth_user', default=lambda: auth.user_id)
)

db.account.user_id.readable = db.account.user_id.writable = False

db.define_table(
    'wallet',
    Field('balance', 'double', default=0, requires=IS_FLOAT_IN_RANGE(0, 1000000)),
    Field('account_id', 'reference account'),
)

db.wallet.account_id.readable = db.wallet.account_id.writable = False

db.define_table(
    'bank',
    Field('bank_name', 'string', requires=IS_IN_SET(('JPMorgan Chase', 'Bank of America', 'Citibank', 'Wells Fargo', 'Goldman Sachs'))),
    Field('card_type', 'string', requires=IS_IN_SET(('AmEx', "Visa", "MCard", "Disc"))),
    Field('card_num', 'string', requires=IS_NOT_EMPTY()),
    Field('ccv', 'string', requires=IS_NOT_EMPTY()),
    Field('exp_month', 'string', requires=IS_NOT_EMPTY()),
    Field('exp_year', 'string', requires=IS_NOT_EMPTY()),
    Field('zip', 'string', requires=IS_NOT_EMPTY()),
    Field('account_id', 'reference account'),
)

db.bank.account_id.readable = db.bank.account_id.writable = False

db.define_table(
    'transaction',
    Field('type', equires=IS_IN_SET(['bank_cashin', 'bank_cashout', 'coff_send', 'coff_req', 'coff_rej', 'coff_acc', 'crypto_buy', 'crypto_sell'])),
    Field('from_id', 'reference account'),
    Field('to_id', 'reference account', default=None),
    Field('bank_id', 'reference bank', default=None),
    Field('amount', 'double', default = 0, requires=IS_FLOAT_IN_RANGE(1, 10000)),
    Field('date_of', 'datetime', requires=IS_NOT_EMPTY()),
    Field('from_balance', 'double', default=None, requires=IS_FLOAT_IN_RANGE(0, 1000000)),
    Field('to_balance', 'double', default=None, requires=IS_FLOAT_IN_RANGE(0, 1000000)),
)

db.define_table(
    'stock_tickers',
    Field('Symbol', 'string'),
    Field('Name', 'string'),
    Field('Country', 'string'),
    Field('IPO', 'integer'),
    Field('Year', 'integer'),
    Field('Volume', 'integer'),
    Field('Sector', 'string'),
    Field('Industry', 'string')
)

db.define_table(
    'crypto_tickers',
    Field('Symbol', 'string'),
    Field('Name', 'string'),
)

db.define_table(
    'crypto_wallet',
    Field('crypto_id', 'reference crypto_tickers'),
    Field('balance', 'double', default=0, requires=IS_FLOAT_IN_RANGE(0, 10000)),
    Field('account_id', 'reference account')
)

db.define_table(
    'crypto_transaction',
    Field('account_id', 'reference account'),
    Field('amount', 'double', default=0, requires=IS_FLOAT_IN_RANGE(0, 1000)),
    Field('crypto_id', 'reference crypto_tickers'),
    Field('transaction_id', 'reference transaction', default=None),
    Field('balance', 'double', default=None, requires=IS_FLOAT_IN_RANGE(0, 10000)),
)

db.define_table(
    'favorites',
    Field('new_friend', 'reference account'),
    Field('user_id', 'reference account'),
)

import csv
if(db(db.stock_tickers).isempty()):
    with open('.\\apps\\coffer_2023\\stock_tickers.csv', 'r', encoding='utf-8', newline='') as dumpfile:
        #for row in dumpfile:
        #    print(row)
        db.import_from_csv_file(dumpfile)
if(db(db.crypto_tickers).isempty()):
    with open('.\\apps\\coffer_2023\\crypto_tickers.csv', 'r', encoding='utf-8', newline='') as dumpfile:
        #for row in dumpfile:
        #    print(row)
        db.import_from_csv_file(dumpfile)

#db.favorites.drop()
#db.crypto_tickers.drop()

db.commit()
