"""
This file defines actions, i.e. functions the URLs are mapped into
The @action(path) decorator exposed the function at URL:

    http://127.0.0.1:8000/{app_name}/{path}

If app_name == '_default' then simply

    http://127.0.0.1:8000/{path}

If path == 'index' it can be omitted:

    http://127.0.0.1:8000/

The path follows the bottlepy syntax.

@action.uses('generic.html')  indicates that the action uses the generic.html template
@action.uses(session)         indicates that the action uses the session
@action.uses(db)              indicates that the action uses the db
@action.uses(T)               indicates that the action uses the i18n & pluralization
@action.uses(auth.user)       indicates that the action requires a logged in user
@action.uses(auth)            indicates that the action requires the auth object

session, db, T, auth, and tempates are examples of Fixtures.
Warning: Fixtures MUST be declared with @action.uses({fixtures}) else your app will result in undefined behavior
"""

import requests
import yfinance as yf
from requests_html import HTMLSession
from yahoo_fin.stock_info import *
from py4web import action, request, abort, redirect, URL, Field
from pydal.validators import *
from yatl.helpers import A
from .common import db, session, T, cache, auth, logger, authenticated, unauthenticated, flash
from py4web.utils.form import Form, FormStyleBulma
from py4web.utils.url_signer import URLSigner
from .models import get_user_email
from datetime import datetime
import random
import re

url_signer = URLSigner(session)

@action('index')
@action.uses('index.html', db, session, auth.user, url_signer)
def index():
    account = db(db.account.user_id == auth.user_id).select(db.account.id, db.account.tag).first()
    if(account == None):
        redirect(URL('account', signer=url_signer))
    wallet = db(db.wallet.account_id == account.id).select().first()
    if(wallet == None):
        db.wallet.insert(account_id = account.id)
        wallet = db(db.wallet.account_id == account.id).select().first()
    print("User:", get_user_email())
    print(globals().get('user'))
    return dict(
        process_request_url = URL('process_request', signer=url_signer),
        load_transactions_url = URL('load_transactions', signer=url_signer),
        get_balance_url = URL('get_balance', signer=url_signer),
        account = account,
        balance = wallet.balance
    )

@action('account', method=["GET", "POST"])
@action.uses('account.html', db, auth.user, url_signer.verify()) #needs to be signed!!!
def account():
    form = Form(db.account, csrf_session=session, formstyle=FormStyleBulma)
    if form.accepted:
        redirect(URL('index'))
    return dict(
        form = form
    )

@action('banks')
@action.uses('banks.html', db, auth.user)
def banks():
    account = db(db.account.user_id == auth.user_id).select(db.account.id, db.account.tag).first()
    user_banks = db(
        (db.bank.account_id == account.id)
    ).select(db.bank.bank_name, db.bank.card_type, db.bank.card_num).as_list()

    for bank in user_banks:
        card_num = bank['card_num']
        new_num = re.sub(r'\d', '*', card_num[0:(len(card_num)-4)]) + card_num[(len(card_num)-4):len(card_num)]
        bank['card_num'] = new_num
    return dict(
        user_banks = user_banks
    )

@action('add_bank')
@action.uses('add_bank.html', db, auth.user, url_signer)
def add_bank():
    currentYear = datetime.now().year
    return dict(add_bank_db_url = URL('add_bank_db', signer=url_signer),
                currentYear=currentYear)

@action('add_bank_db', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def add_bank_db():
    #Package received is not sanitized and is just put into db! I know this is bad!
    account = db(db.account.user_id == auth.user_id).select().first()
    db.bank.insert(
        bank_name = random.choice(['JPMorgan Chase', 'Bank of America', 'Citibank', 'Wells Fargo', 'Goldman Sachs']), # will need to be replaced by somthing that recognizes BID
        card_type = request.json.get('card_type'),
        card_num = request.json.get('card_number'),
        exp_month = request.json.get('exp_month'),
        exp_year = request.json.get('exp_year'),
        ccv = request.json.get('ccv_number'),
        zip = request.json.get('zip_number'),
        account_id = account.id
    )
    return "ok"

@action('load_requests')
@action.uses(db, auth.user, url_signer.verify())
def load_requests():
    account = db(db.account.user_id == auth.user_id).select().first()
    num_loaded = int(request.params.get('num_loaded'))
    outgoing = db((db.transaction.type == "coff_req") & (db.transaction.from_id == account.id)).select(
                                                            limitby=(0, num_loaded), orderby=~db.transaction.date_of).as_list()
    incoming = db((db.transaction.type == "coff_req") & (db.transaction.to_id == account.id)).select(
                                                            limitby=(0, num_loaded), orderby=~db.transaction.date_of).as_list()
    
    for trans in outgoing:
        from_user = db(db.account.id == trans['from_id']).select(db.account.tag).first()
        trans['from_tag'] = from_user.tag
        to_user = db(db.account.id == trans['to_id']).select(db.account.tag).first()
        trans['to_tag'] = to_user.tag
    
    for trans in incoming:
        from_user = db(db.account.id == trans['from_id']).select(db.account.tag).first()
        trans['from_tag'] = from_user.tag
        to_user = db(db.account.id == trans['to_id']).select(db.account.tag).first()
        trans['to_tag'] = to_user.tag
    
    return dict(
        outgoing = outgoing,
        incoming = incoming
    )

@action('load_transactions')
@action.uses(db, auth.user, url_signer.verify())
def load_transactions():
    account = db(db.account.user_id == auth.user_id).select().first()
    num_loaded = int(request.params.get('num_loaded'))
    transactions = db((db.transaction.from_id == account.id) | (db.transaction.to_id == account.id)).select(
                                                                limitby=(0, num_loaded), orderby=~db.transaction.date_of).as_list()
    
    #would rather use left joins but cant figure out how they work in py4web!
    for trans in transactions:
        from_user = db(db.account.id == trans['from_id']).select(db.account.tag).first()
        trans['from_tag'] = from_user.tag

        trans['to_tag'] = None
        if(trans['to_id'] != None):
            to_user = db(db.account.id == trans['to_id']).select(db.account.tag).first()
            trans['to_tag'] = to_user.tag

        trans['bank_name'] = None
        trans['card_num'] = None
        if(trans['bank_id'] != None):
            user_bank = db(db.bank.id == trans['bank_id']).select(db.bank.bank_name, db.bank.card_num).first()
            trans['bank_name'] = user_bank.bank_name
            card_num = user_bank.card_num
            card_cutoff = card_num[(len(card_num)-4):len(card_num)]
            trans['card_num'] = card_cutoff
        
    print("Transactions Loaded:", transactions)
    return dict(transactions = transactions)

@action('submit_transaction', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def submit_transaction():
    account = db(db.account.user_id == auth.user_id).select().first()
    wallet = db(db.wallet.account_id == account.id).select().first()
    bank_id = int(request.json.get('bank_id'))
    bank = db(db.bank.id == bank_id).select().first()
    assert(bank.account_id == account.id)
    amount = float(request.json.get('amount'))
    assert(amount <= 10000 and amount >= 1)
    operation = request.json.get('operation')
    assert(operation == "cashin" or operation == "cashout")
    if(operation == "cashout"): assert(amount <= wallet.balance)
    db.transaction.insert(
            type = ("bank_"+operation),
            bank_id = bank_id,
            from_id = account.id,
            amount = amount,
            date_of = datetime.utcnow(),
            from_balance = wallet.balance
        )
    update = amount if operation == 'cashin' else -amount
    wallet.update_record(balance = wallet.balance + update)
    return "ok"

@action('submit_action', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def submit_action():
    my_account = db(db.account.user_id == auth.user_id).select().first()
    wallet = db(db.wallet.account_id == my_account.id).select().first()
    act_type = request.json.get('type')
    account_list = request.json.get('accounts')
    print(account_list, len(account_list))
    total_amount = 0
    for account in account_list:
        total_amount += float(account['amount'])
    
    assert(len(account_list) > 0 and len(account_list) <= 10)
    assert(len(account_list) == len(set([account['id'] for account in account_list])))
    if(act_type == "coff_send"):
        assert(total_amount < wallet.balance)

    for to_account in account_list:
        amount = float(to_account['amount'])
        assert(amount > 0)
        to_wallet = db(db.wallet.account_id == to_account['id']).select().first()
        db.transaction.insert(type = act_type,
                            from_id = my_account.id,
                            to_id = int(to_account['id']),
                            amount = amount,
                            date_of = datetime.utcnow(),
                            from_balance = wallet.balance if (act_type != "coff_req") else None,  #bal of sender before send takes place
                            to_balance = to_wallet.balance if (act_type != "coff_req") else None)
        if(act_type != "coff_req"):
            wallet.update_record(balance = wallet.balance - amount)
            to_wallet.update_record(balance = to_wallet.balance + amount)
    return "ok"

@action('process_request', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def process_request():
    account = db(db.account.user_id == auth.user_id).select().first()
    wallet = db(db.wallet.account_id == account.id).select().first()
    type = request.json.get('type')
    req_id = request.json.get('req_id')
    print(type, req_id)
    if(type == 'reject'):
        db((db.transaction.type == "coff_req") & (db.transaction.id == req_id) & (db.transaction.to_id == account.id)).delete()
        return "ok"
    elif(type == 'cancel'):
        db((db.transaction.type == "coff_req") & (db.transaction.id == req_id) & (db.transaction.from_id == account.id)).delete()
        return "ok"
    elif(type == 'accept'):
        request_trans = db((db.transaction.type == "coff_req") & (db.transaction.id == req_id) & (db.transaction.to_id == account.id)).select().first()
        assert(request_trans is not None)
        to_wallet = db(db.wallet.account_id == request_trans.from_id).select().first()
        assert(wallet.balance >= request_trans.amount)
        request_trans.update_record(type="coff_acc",
                             date_of = datetime.utcnow(),
                             from_balance = wallet.balance,
                             to_balance = to_wallet.balance
                            )
        wallet.update_record(balance = wallet.balance - request_trans.amount)
        to_wallet.update_record(balance = to_wallet.balance + request_trans.amount)

        result_trans = db((db.transaction.id == req_id) & (db.transaction.to_id == account.id)).select().as_list()
        return dict(
            result_trans = result_trans
        )
    else:
        assert(False)


    return "ok"
        
    
@action('load_banks')
@action.uses(db, auth.user, url_signer.verify())
def load_banks():
    account = db(db.account.user_id == auth.user_id).select().first()
    user_banks = db(db.bank.account_id == account.id).select(db.bank.id, db.bank.card_num,
                                                             db.bank.bank_name, db.bank.card_type).as_list()
    for bank in user_banks:
        card_num = bank['card_num']
        new_num = re.sub(r'\d', '*', card_num[0:(len(card_num)-4)]) + card_num[(len(card_num)-4):len(card_num)]
        bank['card_num'] = new_num
    return dict(
        user_banks=user_banks
    )

@action('load_users')
@action.uses(db, auth.user, url_signer.verify())
def load_users():
    query = request.params.get('query')
    num_users = int(request.params.get('num_users'))
    accounts = []
    if(query == ""):
        accounts = db(db.account.user_id != auth.user_id).select(
                                db.account.id, db.account.tag, db.account.phone, 
                                orderby='<random>', limitby=(0, num_users)).as_list()
    else:
        accounts = db((db.account.user_id != auth.user_id) & (db.account.tag.startswith(query) | 
                      db.account.phone.startswith(query))).select(
                                                db.account.id, db.account.tag, 
                                                db.account.phone, limitby=(0, num_users)).as_list()
    print("Accounts Retrieved", accounts)
    return dict(
        users_loaded = accounts,
    )

@action('get_balance')
@action.uses(db, auth.user, url_signer.verify())
def get_balance():
    account = db(db.account.user_id == auth.user_id).select().first()
    wallet = db(db.wallet.account_id == account.id).select().first()
    return dict(
        user_balance=wallet.balance
    )

@action('transfer/<operation>', method=["GET", "POST"])
@action.uses('transfer.html', db, auth.user, url_signer)
def transfer(operation = None):
    if(operation != "cashin" and operation != "cashout"):
        redirect(URL('index'))
    return dict(
        operation = operation,
        load_banks_url = URL('load_banks', signer=url_signer),
        get_balance_url = URL('get_balance', signer=url_signer),
        submit_transaction_url = URL('submit_transaction', signer=url_signer))
    

@action('send', method=["GET", "POST"])
@action.uses('send.html', db, auth.user, url_signer)
def transfer():
    return dict(
        load_banks_url = URL('load_banks', signer=url_signer),
        load_users_url = URL('load_users', signer=url_signer),
        get_balance_url = URL('get_balance', signer=url_signer),
        submit_send_url = URL('submit_action', signer=url_signer))

@action('request', method=["GET", "POST"])
@action.uses('request.html', db, auth.user, url_signer)
def transfer():
    return dict(
        load_banks_url = URL('load_banks', signer=url_signer),
        load_users_url = URL('load_users', signer=url_signer),
        get_balance_url = URL('get_balance', signer=url_signer),
        submit_request_url = URL('submit_action', signer=url_signer))

@action('incomplete')
@action.uses('incomplete.html', db, auth.user, url_signer)
def incomplete():
    return dict(
        load_requests_url = URL('load_requests', signer=url_signer),
        process_request_url = URL('process_request', signer=url_signer),
    )


#---------------------------------------------------------------------------------------------------------------------------------------------------- 
# Work on favorites system done by Harshwan

@action('favorites')
@action.uses('favorites.html', db, auth.user, url_signer)
def favorites():
    return dict(
                search_url = URL('search', signer=url_signer),
                add_favorite_url = URL('add_favorite', signer=url_signer),
                all_favorites_url = URL('all_favorites', signer=url_signer))

@action('search')
@action.uses(db, auth.user, url_signer.verify())
def search():
    query = request.params.get('query')
    #print(query)
    accounts = []
    if query != "":
        accounts = db((db.account.user_id != auth.user_id) & (db.account.tag.startswith(query)) | 
                      db.account.phone.startswith(query)).select(
                                                db.account.id, db.account.tag, db.account.phone).as_list()
    #print("Accounts Retrieved", accounts)
    return dict(
        users_loaded = accounts,
    )

@action('add_favorite', method='POST')
@action.uses(db, auth.user, url_signer.verify())
def add_favorite():
    account = db(db.account.user_id == auth.user_id).select().first()
    #print("add")
    #print(account.id)
    new_friend = request.json.get('new_friend')
    #print(new_friend)
    db.favorites.insert(
        new_friend=new_friend,
        user_id=account.id,
    )
    return "ok"


@action('all_favorites')
@action.uses(db, auth.user, url_signer.verify())
def all_favorites():
    account = db(db.account.user_id == auth.user_id).select().first()
    assert(account is not None)
    favorited_accounts = db(db.favorites.user_id == account.id).select().as_list()
    favorite_list = []
    #print("favorited_accounts")
    #print(favorited_accounts)
    for friends in favorited_accounts:
        #print("friends")
        #print(friends)
        for key1 in friends:
            if key1 == 'new_friend': 
                favorited_user = friends[key1]
                #print("key: value", key1, favorited_user)
                favorited_user = db(db.account.id == favorited_user).select().first()
                #print("favorited_user")
                #print(favorited_user)
                user={}
                user['id'] = favorited_user.id
                user['tag'] = favorited_user.tag
                user['phone'] = favorited_user.phone
                favorite_list.append(user)
    #print("friends_list")
    #print(favorite_list)
    return dict(friends=favorite_list)

#-----------------------------------------------------------------------------------------------------------------------------------------------------
#CRYPTO STUFF:

@action('crypto_ticker_list')
@action.uses(db, auth.user, url_signer.verify())
def crypto_ticker_list():
    #session = HTMLSession()
    #num_currencies=30
    #resp = session.get(f"https://finance.yahoo.com/crypto?offset=0&count={num_currencies}")
    #tables = pd.read_html(resp.html.raw_html)               
    #df = tables[0].copy()
    #symbols_yf = df.Symbol.tolist()
    #print(symbols_yf[:30])

    crypto_list = db(db.crypto_tickers).select()
    print(crypto_list)

    return dict(
        crypto_list = crypto_list
    )

@action('get_ticker_data')
@action.uses(db, auth.user, url_signer.verify())
def get_crypto_data():
    Symbol =  request.params.get('Symbol')
    #ticker =  yf.Ticker(Symbol)
    assert(Symbol is not None)
    print(Symbol)
    quote_dict = get_quote_table(Symbol)
    assert(quote_dict is not None)
    return dict(quote_price =  round(quote_dict['Quote Price'], 2),
                day_chg = round((((quote_dict['Quote Price'] - quote_dict['Open']) / quote_dict['Open']) * 100), 2),
                mkt_cap = quote_dict['Market Cap'],
                open = round(quote_dict['Open'], 2),
                prev_close = round(quote_dict['Previous Close'], 2),
                volume =  quote_dict['Volume'],
                day_range = quote_dict["Day's Range"],
                )

@action('crypto_store')
@action.uses('crypto_store.html', db, auth.user, url_signer)
def crypto_store():
    #crypto_list = tickers_nasdaq() #tickers_dow() #get_top_crypto()
    #print(crypto_list)

    return dict(
        crypto_ticker_list_url = URL('crypto_ticker_list', signer=url_signer),
        get_ticker_data_url = URL('get_ticker_data', signer=url_signer),
        get_crypto_link_url = URL('get_crypto_link'),
        )

@action('crypto/<id:int>')
@action.uses('crypto.html', db, auth.user)
def crypto(id=None):
    if(id is None):
        redirect(URL('index'))
    else:
        crypto = db(db.crypto_tickers.id == id).select().first()
        if (crypto is None):
            redirect(URL('index'))

        return dict(
            get_ticker_data_url = URL('get_ticker_data', signer=url_signer),
            get_chart_data_url = URL('get_chart_data', signer=url_signer),
            get_crypto_bal_url = URL('get_crypto_bal', signer=url_signer),
            Symbol = crypto.Symbol,
            Name = crypto.Name,
            id = crypto.id,
            )

@action('get_crypto_link')
@action.uses(db, auth.user)
def get_crypto_link():
    crypto_id = request.params.get('crypto_id')
    return dict(crypto_url=URL('crypto', crypto_id))

@action('get_chart_data')
@action.uses(db, auth.user, url_signer.verify())
def get_chart_data():
    period = request.params.get('period')
    interval = request.params.get('interval')
    Symbol = request.params.get('Symbol')
    assert((period is not None) and (interval is not None) and (Symbol is not None))
    ticker = yf.Ticker(Symbol)
    data = ticker.history(period=period, interval=interval) #5d, 30m
    
    datetime_list =[]

    # iterating the columns
    for timestamp in data.index:
        date = timestamp.to_pydatetime()
        datetime_list.append(date.replace(tzinfo=None))

    price_list = data['Close'].tolist()
    print(datetime_list)
    #print(datetime.utcnow())
    print(price_list)
    print(data)
    print(type(data))

    return dict(datetime_list = datetime_list,
                price_list = price_list)

@action('crypto_trans/<id:int>')
@action.uses('crypto_trans.html', db, auth.user)
def crypto_trans(id=None):
    if(id is None):
        redirect(URL('index'))
    else:
        crypto = db(db.crypto_tickers.id == id).select().first()
        if (crypto is None):
            redirect(URL('index'))

        return dict(
            get_ticker_data_url = URL('get_ticker_data', signer=url_signer),
            submit_crypto_transaction_url = URL('submit_crypto_transaction', signer=url_signer),
            get_balance_url = URL('get_balance', signer=url_signer),
            get_crypto_bal_url = URL('get_crypto_bal', signer=url_signer),
            Symbol = crypto.Symbol,
            Name = crypto.Name,
            id = crypto.id,
        )

@action('get_crypto_bal')
@action.uses(db, auth.user, url_signer.verify())
def get_balance():
    crypto_id = request.params.get('crypto_id')
    account = db(db.account.user_id == auth.user_id).select().first()
    crypto_wallet = db((db.crypto_wallet.account_id == account.id) & (db.crypto_wallet.crypto_id == crypto_id)).select().first()
    if(crypto_wallet is None):
        return dict(crypto_bal = 0)
    else:
        return dict(
            crypto_bal=crypto_wallet.balance
        )

#dict(crypto_list=crypto_list)

@action('submit_crypto_transaction', method="POST")
@action.uses(db, auth.user, url_signer.verify())
def submit_transaction():
    amount_usd = float(request.json.get('amount_usd'))
    crypto_id = int(request.json.get('crypto_id'))
    crypto_ticker = db(db.crypto_tickers.id == crypto_id).select().first()
    assert(crypto_ticker is not None)
    operation = request.json.get('operation')
    assert(operation == "buy" or operation == "sell")
    account = db(db.account.user_id == auth.user_id).select().first()
    crypto_wallet = db((db.crypto_wallet.account_id == account.id) & (db.crypto_wallet.crypto_id == crypto_id)).select().first()
    wallet = db(db.wallet.account_id == account.id).select().first()

    quote_dict = get_quote_table(crypto_ticker.Symbol)
    price = quote_dict['Quote Price']
    amount = amount_usd/price #the amount of crypto being bought or sold.
    if(operation == "buy"): 
        assert(amount_usd <= wallet.balance and amount_usd > 1)
    elif(operation == "sell"): 
        assert(crypto_wallet is not None)
        assert(amount <= crypto_wallet.balance)
    trans_id = db.transaction.insert(
            type = ("crypto_"+operation),
            from_id = account.id,
            amount = amount_usd,
            date_of = datetime.utcnow(),
            from_balance = wallet.balance
        )
    #db.crypto_transaction.insert(
        #    account_id = account.id,
        #    amount = 
        #)
    
    if(crypto_wallet is None and operation == 'buy'):
        db.crypto_wallet.insert(
            crypto_id = crypto_id,
            balance = amount,
            account_id = account.id
        )
        wallet.update_record(balance = wallet.balance - amount_usd)
    elif(operation == 'buy'):
        crypto_wallet.update_record(balance = (crypto_wallet.balance + amount))
        wallet.update_record(balance = wallet.balance - amount_usd)
    elif(operation == "sell"):
        crypto_wallet.update_record(balance = crypto_wallet.balance - amount)
        wallet.update_record(balance = wallet.balance + amount_usd)
    

    return "ok"