// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        id: null,
        Symbol: null,
        Name: null,
        balance: 0,
        crypto_bal: 0,
        operation: null,
        amount_error: false, 
        amount: "0.0",
        amount_usd: "0.0",
        quote_price: null,
    };
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.validate_op = function(){
        if(app.vue.operation != "buy" && app.vue.operation != "sell"){
            window.location = index_url;
        }
    }

    app.validate_amnt = function(){
        if(app.vue.operation == "buy" && app.vue.amount_usd > app.vue.balance){
            app.vue.amount_error = true;
        }
        else if(app.vue.operation == "sell" && app.vue.amount > app.vue.crypto_bal){
            app.vue.amount_error = true;
        }
    }

    app.edit_amnt_usd = function(){
        //try {
            app.vue.amount_error = false;

            amnt_reg = /(\d*)([\.]\d*)?/
            cleaned = app.vue.amount_usd;
            var match = cleaned.match(amnt_reg);
            var dec_list = [match[1], match[2], match[3]];
            app.vue.amount_usd = dec_list.join("");
            if(app.vue.amount_usd != null && app.vue.amount_usd >= 1){
                app.vue.amount = app.vue.amount_usd / app.vue.quote_price;
            }else{
                app.vue.amount = 0
            }
    }

    app.edit_amnt = function(){
        //try {
            app.vue.amount_error = false;

            amnt_reg = /(\d*)([\.]\d*)?/
            cleaned = app.vue.amount;
            var match = cleaned.match(amnt_reg);
            var dec_list = [match[1], match[2], match[3]];
            app.vue.amount = dec_list.join("");
            app.vue.amount_usd = app.vue.amount * app.vue.quote_price;
    }

    app.get_ticker_data = function(){
        axios.get(get_ticker_data_url, {params: {Symbol: app.vue.Symbol}}).then(function (response) {
            app.vue.quote_price = response.data.quote_price;
        });
    }

    app.get_crypto_bal = function(){
        axios.get(get_crypto_bal_url, {params: {crypto_id: app.vue.id}}).then(function (response) {
            app.vue.crypto_bal = response.data.crypto_bal;
        });
    }

    app.get_balance = function(){
        axios.get(get_balance_url).then(function (response) {
            app.vue.balance = response.data.user_balance;
        });
    }

    app.submit_transaction = function(operation){
        app.vue.operation = operation;
        app.vue.balance = app.get_balance();
        app.validate_amnt();
        app.validate_op();
        if(!app.vue.amount_error){
            axios.post(submit_crypto_transaction_url,
                {operation: app.vue.operation,
                crypto_id: app.vue.id,
                amount_usd: app.vue.amount_usd,
                }).then(function (response){
                    window.location = index_url;
                })
        }
        
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        edit_amnt: app.edit_amnt,
        get_balance: app.get_balance,
        edit_amnt_usd: app.edit_amnt_usd,
        submit_transaction: app.submit_transaction
        //submit_transaction: app.submit_transaction,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.vue.id = crypto_id;
        app.vue.Symbol = crypto_symbol;
        app.vue.Name = crypto_name;
        app.get_ticker_data();
        app.get_balance();
        app.get_crypto_bal();
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
