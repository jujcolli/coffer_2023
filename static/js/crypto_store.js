// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        crypto_list: []
        // Complete as you see fit.
    };    
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.get_tickers = function(){
        axios.get(crypto_ticker_list_url).then(function (response) {
            app.vue.crypto_list = app.enumerate(response.data.crypto_list);
            app.load_all();
        });
    }

    app.load_all = function(){
        for (let i = 0; i < app.vue.crypto_list.length; i++){
            axios.get(get_ticker_data_url, {params: {Symbol: app.vue.crypto_list[i]['Symbol']}}).then(function (response) {
                Vue.set(app.vue.crypto_list[i], 'quote_price', response.data.quote_price)
                Vue.set(app.vue.crypto_list[i], 'day_chg', response.data.day_chg)
                Vue.set(app.vue.crypto_list[i], 'mkt_cap',  response.data.mkt_cap)
            });
        }
    }

    app.goto_crypto = function(crypto_id){
        axios.get(get_crypto_link_url , {params: {crypto_id: crypto_id}}).then(function (response) {
            crypto_link = response.data.crypto_url;
            window.location = crypto_link;
        });
    }
    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        goto_crypto: app.goto_crypto
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.get_tickers();
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
