// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        custom: false,
        balance: 0,
        amount_error: false,
        amount: "0.0",
        total_amount: 0.0,
        prev_amount: "0.0",
        user_dropdown: false,
        recipient_error: false,
        user_query: "",
        num_users: 15,
        users_loaded: [],
        recipient_list: [],
    };

    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.startCustom = function(){
        if(app.vue.recipient_list.length < 2){return;}
        app.vue.custom = true;
    }

    app.stopCustom = function(){
        app.vue.custom = false;
        for(recipient of app.vue.recipient_list){
            recipient.amount = app.vue.amount;
        }
        app.update_total();
    }

    app.validate_amnt = function(){
        app.vue.amount_error = false;
        if(app.vue.total_amount > 10000 || app.vue.total_amount < 1){
            app.vue.amount_error = true;
            return;
        }
        for(recipient of app.vue.recipient_list){
            if(recipient.amount <= 0){
                app.vue.amount_error = true;
                return;
            }
        }
    }
    
    app.verify_recipients = function(){
        app.vue.recipient_error = false;
        if(app.vue.recipient_list < 1 || app.vue.recipient_list > 10){
            app.vue.recipient_error = true;
            app.vue.custom = false;
        }
    }

    app.update_total = function(){ //only to ever be used right after adding to list
        if(!app.vue.custom){
            app.vue.total_amount = app.vue.amount * app.vue.recipient_list.length
        }else if(app.vue.custom){
            app.vue.total_amount = 0;
            for(recipient of app.vue.recipient_list){
                app.vue.total_amount += parseFloat(recipient.amount);
            }
        }
    }

    app.add_recipient = function(user_idx){
        if(app.vue.recipient_list.length > app.vue.max_recipients){return}
        new_recipient = app.vue.users_loaded[user_idx];
        //if(!(app.vue.recipient_list).includes(new_recipient)){return;}
        for (recipient of app.vue.recipient_list){
            if(recipient.user.id == new_recipient.id){
                app.vue.user_dropdown = false;
                app.reset_query();
                return;
            }
        }
        app.vue.recipient_list.push(
            {_idx: app.vue.recipient_list.length, user: new_recipient, amount: app.vue.amount});
        app.vue.user_dropdown = false;
        app.reset_query();
        app.update_total();
        app.validate_amnt();
        app.verify_recipients(); //shouldnt be necessary but added it just in case
    };

    app.custom_editRecipient = function(){
        app.update_total();
        app.validate_amnt();
    };

    app.remove_recipient = function(recipient_idx){
        app.vue.recipient_list.splice(recipient_idx, 1);
        app.update_total();
        app.validate_amnt();
        app.verify_recipients();
    };

    app.reset_query = function(){
        app.vue.user_query = "";
    };

    app.edit_amnt = function(){
        //try {
            app.vue.amount_error = false;

            amnt_reg = /(\d{0,5})([\.]\d{0,2})?/
            cleaned = app.vue.amount;
            var match = cleaned.match(amnt_reg);
            var dec_list = [match[1], match[2], match[3]];
            app.vue.amount = dec_list.join("");
            if(app.vue.amount > 10000 || app.vue.amount < 0){
                app.vue.amount = app.vue.prev_amount;
            }
            app.vue.prev_amount = app.vue.amount;

            for(recipient of app.vue.recipient_list){
                recipient.amount = app.vue.amount;
            }

            app.update_total();
            app.validate_amnt();
    }

    app.edit_custom_amnt = function(recip_idx){
            if(!app.vue.custom){return;}
            recipient = app.vue.recipient_list[recip_idx];

            amnt_reg = /(\d{0,5})([\.]\d{0,2})?/
            cleaned = recipient.amount;
            var match = cleaned.match(amnt_reg);
            var dec_list = [match[1], match[2], match[3]];
            recipient.amount = dec_list.join("");
            if(recipient.amount > 10000){
                recipient.amount = 10000;
            }else if(recipient.amount < 0){
                recipient.amount = 0;
            }

            app.update_total();
            app.validate_amnt();
    }

    app.get_users = function(){
        axios.get(load_users_url, {params : {query: app.vue.user_query, num_users: app.vue.num_users}}).then(function (response) {
            app.vue.users_loaded = app.enumerate(response.data.users_loaded);
        });
    };

    app.toggle_dropdown = function(){
        app.vue.user_dropdown = !app.vue.user_dropdown;
    };

    app.request = function(){
        app.validate_amnt();
        app.verify_recipients();
        if(!app.vue.amount_error && !app.vue.recipient_error){
            id_list = []
            for(recipient of app.vue.recipient_list){
                id_list.push({id:recipient.user.id, amount:recipient.amount});
            }
            axios.post(submit_request_url,
                {
                type: "coff_req",
                accounts: id_list,
                }).then(function (response){
                    window.location = index_url;
                })
        }
        
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        request: app.request,
        edit_amnt: app.edit_amnt,
        get_users: app.get_users,
        reset_query: app.reset_query,
        add_recipient: app.add_recipient,
        remove_recipient: app.remove_recipient,
        toggle_dropdown: app.toggle_dropdown,
        startCustom: app.startCustom,
        stopCustom: app.stopCustom,
        edit_custom_amnt: app.edit_custom_amnt,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.get_users();
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
