// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        balance: 0,
        operation: null,
        selected_bank: null,
        amount_error: false, 
        bank_error: false,
        amount: "0.0",
        prev_amount: "0.0",
        bank_dropdown: false,
        addbank_popup: false,
        // Complete as you see fit.
        user_banks: [],
    };
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.toggleOn_addbank = function(){
        app.vue.addbank_popup = true;
    }

    app.toggleOff_addbank = function(){
        app.vue.addbank_popup = false;
    }

    app.validate_op = function(){
        if(app.vue.operation != "cashin" && app.vue.operation != "cashout"){
            window.location = index_url;
        }
    }

    app.validate_amnt = function(){
        if(app.vue.amount > 10000 || app.vue.amount < 1){
            app.vue.amount_error = true;
        }
        if(app.vue.operation == "cashout" && app.vue.amount > app.vue.balance){
            app.vue.amount_error = true;
        }
    }

    app.set_bank = function(bank_idx){
        app.vue.selected_bank = app.vue.user_banks[bank_idx];
        app.vue.bank_dropdown = false;
        app.vue.bank_error = false;
    };

    app.edit_amnt = function(){
        //try {
            app.vue.amount_error = false;

            amnt_reg = /(\d{0,5})([\.]\d{0,2})?/
            cleaned = app.vue.amount;
            var match = cleaned.match(amnt_reg);
            var dec_list = [match[1], match[2], match[3]];
            app.vue.amount = dec_list.join("");
            if(app.vue.amount > 10000 || app.vue.amount < 0){
                app.vue.amount = app.vue.prev_amount;
            }
            app.vue.prev_amount = app.vue.amount;

            app.validate_amnt();
    }

    app.get_banks = function(){
        axios.get(load_banks_url).then(function (response) {
            app.vue.user_banks = app.enumerate(response.data.user_banks);
            if(app.vue.user_banks.length > 0){
                app.vue.selected_bank = app.vue.user_banks[0];
            }
        });
    };

    app.toggle_dropdown = function(){
        app.vue.bank_dropdown = !app.vue.bank_dropdown;
    };

    app.get_balance = function(){
        axios.get(get_balance_url).then(function (response) {
            app.vue.balance = response.data.user_balance;
        });
    }

    app.submit_transaction = function(){
        app.vue.balance = app.get_balance();
        app.validate_amnt();
        app.validate_op();
        if(app.vue.selected_bank == null){
            app.vue.bank_error = true;
        }
        if(!app.vue.amount_error && !app.vue.bank_error){
            axios.post(submit_transaction_url,
                {bank_id: app.vue.selected_bank.id,
                operation: app.vue.operation,
                amount: app.vue.amount,
                }).then(function (response){
                    window.location = index_url;
                })
        }
        
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        edit_amnt: app.edit_amnt,
        get_banks: app.get_banks,
        get_balance: app.get_balance,
        set_bank: app.set_bank,
        toggle_dropdown: app.toggle_dropdown,
        submit_transaction: app.submit_transaction,
        toggleOn_addbank: app.toggleOn_addbank,
        toggleOff_addbank: app.toggleOff_addbank
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.get_banks();
        app.get_balance();
        app.vue.operation = operation;
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
