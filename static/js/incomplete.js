// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        // Complete as you see fit.
        incoming: [],
        outgoing: [],
    };
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.get_requests = function(){
        axios.get(load_requests_url, {params: {num_loaded: 10}}).then(function (response) {
            app.vue.incoming = app.enumerate(response.data.incoming);
            app.vue.outgoing = app.enumerate(response.data.outgoing);
        });
    }

    app.accept = function(trans_idx){
        request = app.vue.incoming[trans_idx]
        app.vue.incoming.splice(trans_idx, 1);
        axios.post(process_request_url,
            {
            type: "accept",
            req_id: request.id,
            }).then(function(response){
                return;
            });
    };

    app.reject = function(trans_idx){
        request = app.vue.incoming[trans_idx]
        app.vue.incoming.splice(trans_idx, 1);
        axios.post(process_request_url,
            {
            type: "reject",
            req_id: request.id,
            }).then(function (response){
                return;
            })
    };

    app.cancel = function(trans_idx){
        request = app.vue.outgoing[trans_idx]
        app.vue.outgoing.splice(trans_idx, 1);
        axios.post(process_request_url,
            {
            type: "cancel",
            req_id: request.id,
            }).then(function (response){
                return;
            })
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        cancel: app.cancel,
        reject: app.reject,
        accept: app.accept,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.get_requests();
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
