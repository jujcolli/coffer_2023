// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        // Complete as you see fit.
        num_loaded: 10,
        transaction_list: [],
        account_id: null,
        balance: 0,
    };    
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };    

    app.get_transactions = function(){
        axios.get(load_transactions_url, {params: {num_loaded: app.vue.num_loaded}}).then(function (response) {
            app.vue.transaction_list = app.enumerate(response.data.transactions);
        });
    }

    app.get_balance = function(){
        axios.get(get_balance_url).then(function (response) {
            app.vue.balance = response.data.user_balance;
        });
    }

    app.accept = function(trans_idx){
        request = app.vue.transaction_list[trans_idx]
        //console.log(request.amount <= app.vue.balance)
        if (request.amount <= app.vue.balance){
            app.vue.transaction_list.splice(trans_idx, 1);
            axios.post(process_request_url,
                {
                type: "accept",
                req_id: request.id,
                }).then(function(response){
                    result_trans = response.data.result_trans
                    app.vue.transaction_list = app.enumerate(result_trans.concat(app.vue.transaction_list));
                    app.get_balance()
                });
        }
    };

    app.reject = function(trans_idx){
        request = app.vue.transaction_list[trans_idx]
        app.vue.transaction_list.splice(trans_idx, 1);
        axios.post(process_request_url,
            {
            type: "reject",
            req_id: request.id,
            }).then(function (response){
                return;
            })
    };

    app.cancel = function(trans_idx){
        request = app.vue.transaction_list[trans_idx]
        app.vue.transaction_list.splice(trans_idx, 1);
        axios.post(process_request_url,
            {
            type: "cancel",
            req_id: request.id,
            }).then(function (response){
                return;
            })
    };

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        reject: app.reject,
        accept: app.accept,
        cancel: app.cancel,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.get_transactions();
        app.get_balance();
        app.vue.account_id = account_id;
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
