// This will be the object that will contain the Vue attributes
// and be used to initialize it.
//import { Chart } from "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"

let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        crypto_bal: 0.0,
        quote_price: null,
        day_range: null,
        day_chg: null,
        mkt_cap: null,
        open: null,
        prev_close: null,
        volume: null,
        ticker_id: null,
        Symbol: null,
        chart: null,
        price_list: [],
        datetime_list: []
        // Complete as you see fit.
    };    
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.get_chart_data =function(period="1d", interval="5m"){
        axios.get(get_chart_data_url, {params: {Symbol: app.vue.Symbol, period: period, interval: interval}}).then(function (response) {
            //app.vue.datetime_list = response.data.datetime_list;
            datetime_list_UTC = response.data.datetime_list;
            app.vue.datetime_list = []
            for (UTC_datetime of datetime_list_UTC){
                relative_date = Sugar.Date(UTC_datetime + "Z").relative()
                app.vue.datetime_list.push(relative_date)
            }
            app.vue.price_list = response.data.price_list;

            app.vue.chart.data.labels = app.vue.datetime_list;
            app.vue.chart.data.datasets[0].data = app.vue.price_list;
            app.vue.chart.update();
        });
    }

    app.make_chart = function(){
        //const xValues = [100,200,300,400,500,600,700,800,900,1000];

        app.vue.chart = new Chart("myChart", {
        type: "line",
        data: {
            labels: app.vue.datetime_list, //xValues,
            datasets: [{ 
            data: app.vue.price_list,//[1600,1700,1700,1900,2000,2700,4000,5000,6000,7000],
            borderColor: 'rgb(75, 192, 192)',
            fill: false,
            tension: 0.1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {display: false}
        }
        });
    }

    app.get_crypto_bal = function(){
        console.log(app.vue.ticker_id)
        axios.get(get_crypto_bal_url, {params: {crypto_id: app.vue.ticker_id}}).then(function (response) {
            app.vue.crypto_bal = response.data.crypto_bal;
        });
    }

    app.get_ticker_data = function(){
        axios.get(get_ticker_data_url, {params: {Symbol: app.vue.Symbol}}).then(function (response) {
            app.vue.quote_price = response.data.quote_price;
            app.vue.day_chg = response.data.day_chg;
            app.vue.mkt_cap = response.data.mkt_cap;
            app.vue.open = response.data.open;
            app.vue.prev_close = response.data.prev_close;
            app.vue.volume = response.data.volume;
            app.vue.day_range = response.data.day_range;
        });
    }

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        get_chart_data: app.get_chart_data
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
        app.vue.ticker_id = crypto_id;
        app.vue.Symbol = crypto_symbol;
        app.make_chart();
        app.get_chart_data();
        app.get_crypto_bal();
        app.get_ticker_data();
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
