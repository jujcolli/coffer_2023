// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {

    // This is the Vue data.
    app.data = {
        exp_month: null,
        exp_year: null,
        user_number: "",
        ccv_number: "",
        zip_number: "",
        req_length: 16,
        ccv_length: 3,
        card_type: "None",
        number_error: false,
        ccv_error: false,
        zip_error: false,
        exp_error: false,
        // Complete as you see fit.
    };    
    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    //this is adapted from: https://taimoorsattar.com/blogs/format-input-text-while-typing-javascript
    app.format_number= function(regex, cleaned) {
        try {
          var match = cleaned.match(regex);
          //var bin = match[1];
          var num_list = [match[1]]
          for(i=2; i < match.length; i++){
            num_list.push(match[i] ? " ": "")
            num_list.push(match[i])
          }
          app.vue.user_number = num_list.join("")
         
        } catch(err) {
            app.vue.user_number = "";
            app.vue.card_type = "None"
        }
    }

    app.fix_number = function(){
        regex1 = /^(\d{0,4})?(\d{0,4})?(\d{0,4})?(\d{0,4})$/;
        regex2 = /^(\d{0,4})?(\d{0,6})?(\d{0,5})$/;
        
        types = {"AmEx": {regex: /^(3[47])/, length: 15, ccv_len: 4, format: regex2},
                 "Visa": {regex: /^(4)/, length: 16, ccv_len: 3, format: regex1},
                 "MCard": {regex: /^(5[1-5])/, length: 16, ccv_len: 3, format: regex1},
                 "Disc": {regex: /^(6601)/, length: 16, ccv_len: 3, format: regex1},
                };

        var cleaned = ("" + app.vue.user_number).replace(/\D/g, "").slice(0, app.vue.req_length);
        app.vue.card_type = "None";
        format_reg = regex1;
        for (let k in types){
            regex = types[k]["regex"]
            if(regex.test(cleaned)){
                app.vue.card_type = k;
                format_reg = types[k]["format"];
                app.vue.req_length = types[k]["length"];
                app.vue.ccv_length = types[k]["ccv_len"]
                app.vue.number_error = false;
            }
        }
        if(app.vue.card_type == "None"){
            app.vue.req_length = 16;
            app.vue.ccv_length = 3;
            app.vue.number_error = true;
        }
        app.format_number(format_reg, cleaned)
    }

    app.fix_ccv = function(){
        var cleaned = ("" + app.vue.ccv_number).replace(/[^0-9]/g, "");
        app.vue.ccv_number = cleaned.slice(0, app.vue.ccv_length);
    }

    app.fix_zip = function(){
        var cleaned = ("" + app.vue.zip_number).replace(/[^0-9]/g, "");
        app.vue.zip_number = cleaned.slice(0, 5);
    }

    app.add_bank = function(){
        var cleaned = ("" + app.vue.user_number).replace(/\D/g, "");
        app.vue.number_error = (cleaned.length != app.vue.req_length || app.vue.card_type == "None") ? true : false;
        app.vue.ccv_error = (app.vue.ccv_number.length != app.vue.ccv_length) ? true : false;
        app.vue.zip_error = (app.vue.zip_number.length != 5) ? true : false;
        app.vue.exp_error = (app.vue.exp_month == null || app.vue.exp_year == null) ? true : false;
        if(!app.vue.number_error && !app.vue.ccv_error && !app.vue.zip_error && !app.vue.exp_error){
            axios.post(add_bank_db_url,
                {
                    card_type: app.vue.card_type,
                    card_number: app.vue.user_number,
                    exp_month : app.vue.exp_month,
                    exp_year : app.vue.exp_year,
                    ccv_number : app.vue.ccv_number,
                    zip_number : app.vue.zip_number
                }).then(function (response){
                    window.location = go_banks_url;
                })
        }
    }

    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        fix_ccv: app.fix_ccv,
        fix_number: app.fix_number,
        format_number: app.format_number,
        fix_zip: app.fix_zip,
        add_bank: app.add_bank,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods
    });

    // And this initializes it.
    app.init = () => {
        // Put here any initialization code.
    };

    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
