// This will be the object that will contain the Vue attributes
// and be used to initialize it.
let app = {};

// Given an empty app object, initializes it filling its attributes,
// creates a Vue instance, and then initializes the Vue instance.
let init = (app) => {
    
    // This is the Vue data.
    app.data = {
        user_dropdown: false,
        user_query: "",
        users_loaded: [],
        friends: [],
    };  

    
    app.enumerate = (a) => {
        // This adds an _idx field to each element of the array.
        let k = 0;
        a.map((e) => {e._idx = k++;});
        return a;
    };

    app.reset_query = function(){
        app.vue.user_query = "";
    }

    app.get_fav = function(){
        axios.get(search_url, {params : {query: app.vue.user_query}})
            .then(function (response) {
                console.log(app.vue.user_query)
                app.vue.users_loaded = app.enumerate(response.data.users_loaded);
            });
    };
    
    app.all_fav = function(){
        axios.get(all_favorites_url, {})
            .then(function (response) {
                app.vue.friends = app.enumerate(response.data.friends);
                console.log(app.vue.friends)
            });
    }

    app.add_favorite = function(user){
        //Hotfix!
        for (let i = 0; i < app.vue.friends.length; i++){
            console.log("FRIEND:", app.vue.friends[i])
            if(app.vue.friends[i]['id'] == user.id){
                return;
            }
        }
        new_recipient = user.id;
        console.log("NEW_Recipient", new_recipient)
        axios.post(add_favorite_url,
            {
                new_friend:user.id
            }).then(function (response) {
                app.vue.friends.push(user);
            });
    };
    // This contains all the methods.
    app.methods = {
        // Complete as you see fit.
        get_fav: app.get_fav,
        reset_query: app.reset_query,
        add_favorite: app.add_favorite,
        all_fav: app.all_fav,
    };

    // This creates the Vue instance.
    app.vue = new Vue({
        el: "#vue-target",
        data: app.data,
        methods: app.methods,
    });
    app.init = () => {
        app.all_fav();
        // Put here any initialization code.
        // Typically this is a server GET call to load the data.
    };
    // Call to the initializer.
    app.init();
};

// This takes the (empty) app object, and initializes it,
// putting all the code in it. 
init(app);
